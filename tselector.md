# The TSelector

The slow, ugly `MakeClass` tools have been replaced by the much more elegant `TSelector` framework, with a similar template created using `MakeSelector`. The following tutorial will show you how to use this system to prepare an analysis.

## Creating the template files

Open your file and get access to your `TTree`. Run the `->MakeSelector("NameOfFiles")` method on your `TTree` to make two files, `NameOfFiles.C` and `NameOfFiles.h`, that you can edit.

## Editing the files

The files contain nicely organized places to put your analysis code. Here are the main parts you'll want to add:

### Definitions

You'll want to define your histograms in one of two places. The best place is in the header file, somewhere after the `public:` line in the class. If you really want to keep all edits to one file, you can also define the histograms at the top of the `.C` file, before the functions start[^1]. As long as you don't have a massive amount of work to do and need to use PROOF[^2], this will work. In either location, this example preparing two histograms would work:

```cpp
TH1D *LamMLL = nullptr;
TH2D *LamMDD = nullptr;
```

You don't have to set the pointers to `nullptr`[^3], but this is a good habit.

### Preparation

There are two methods prepared for you for initialization, one for called "Begin" and one called "SlaveBegin". As a general rule, put your histogram initialization in `SlaveBegin`[^4].

```cpp
LamMLL = new TH1D("LamMLL", "Lambda mass LL", 150, 1108.5, 1122.5); 
LamMDD = new TH1D("LamMDD", "Lambda mass DD", 150, 1108.5, 1122.5); 

fOutput->Add(LamMLL);
fOutput->Add(LamMDD);
```

The final two lines register your histograms with the PROOF server, and are optional if you are not using PROOF.

### The Looping

The `Process` method gets called once for each event; the argument `entry` is the current entry number. The details of the loop itself are handled for you. Before your code you'll want to add the line:

```cpp
GetEntry(entry);
```

This calls a member function (`this->GetEntry`) with the current event number, preparing all the branches to be read. However, the actual read into memory only occurs when you access the value for the first time, which is *really* useful for fast processing on large NTuples.

To access a value, use the branch name, prefixed with a star[^5]. Continuing our example:

```cpp
if(*DecayPr_TRACK_Type == 3)
   LamMLL->Fill(*Lambda_M);
else if (*DecayPr_TRACK_Type == 5)
   LamMDD->Fill(*Lambda_M);
```

You'll probably have more histograms and more if statements in your code, but the ideas are the same.

### Final processing

The finalization can, again, happen in two places. In most cases, you will be using this place to make plots or save results, and for that you'll use the `Terminate` method (`SlaveTerminate` is almost always empty). For example, you might want to plot the histograms you just made:

```cpp
LamMDD->Draw("");
LamMLL->Draw("SAME");
```

## Running the template file

One great thing about MakeSelector is that you can run it on any similar `TTree` or even `TChain`; the original name and location are not part of the files created. It does mean that there are several steps in picking a file and running it that you will have to do in ROOT. Here is a little ROOT script that would run the `TSelector` for you:

```cpp
{
auto _file0 = TFile::Open("DVntuple-March04A.root");
myXiTuple->cd();
XiDecayTuple->Process("Xi_Selector.C");
}
```
You can compile the selector by adding a `+` to the end of the filename.

## Speed comparison

Running code similar to what is listed above for several different methods gives the following times:

| Method | Time | Notes |
|--------|------|-------|
| `MakeClass` | 49.3 s  | Every entry must be read into memory. Could be made faster by manually blocking out unused values. |
| `MakeSelector` | 3.56 s | Everything left to default, as above. |
| Manually reading 2 branches | 3.30 s | Only a hair better than `TSelector`! |
| PROOF `MakeSelector`, 48 workers | 34.9 s | Massive overhead, all workers reading the same file, and transferring results back together. |

## Bonus: Reading the branches manually

If you'd like to see how to do this without using a template, that is included here. I've also included the full graphical options that I used in the tests.

{% codetabs name="Manual", type="cpp" %}{
auto LamMLL = new TH1D("LamMLL", "Lambda mass LL", 150, 1108.5, 1122.5); 
auto LamMDD = new TH1D("LamMDD", "Lambda mass DD", 150, 1108.5, 1122.5); 

auto _file0 = TFile::Open("DVntuple-March04A.root");
TTreeReader myReader("myXiTuple/XiDecayTuple", _file0);

TTreeReaderValue<Double_t> Lambda_M(myReader, "Lambda_M");
TTreeReaderValue<Int_t> DecayPr_TRACK_Type(myReader, "DecayPr_TRACK_Type");

while(myReader.Next()) {
   if(*DecayPr_TRACK_Type == 3)
       LamMLL->Fill(*Lambda_M);
   else if (*DecayPr_TRACK_Type == 5)
       LamMDD->Fill(*Lambda_M);
}

TCanvas *C = new TCanvas();

gStyle->SetOptTitle(kFALSE);
gStyle->SetOptStat(0);

LamMDD->Draw("");
LamMLL->Draw("SAME");
gPad->BuildLegend();
}
{% language name="RDataFrame", type="cpp" %}{
ROOT::EnableImplicitMT();

using namespace ROOT;

RDataFrame frame("myXiTuple/XiDecayTuple", "DVntuple-March04A.root");

auto LamMLL = frame
    .Filter([](int v){return v == 3;}, {"DecayPr_TRACK_Type"})
    .Histo1D({"lammll", "#Lambda M LL", 150, 1108.5, 1122.5}, "Lambda_M");

auto LamMDD = frame
    .Filter([](int v){return v == 5;}, {"DecayPr_TRACK_Type"})
    .Histo1D({"lammdd", "#Lambda M DD", 150, 1108.5, 1122.5}, "Lambda_M");

TCanvas *C = new TCanvas();
gStyle->SetOptTitle(kFALSE);
gStyle->SetOptStat(0);

LamMDD->Draw("");
LamMLL->Draw("SAME");
gPad->BuildLegend();
}
{% endcodetabs %}


[^1]: This is called a global variable and is evil. But it is still less evil than `MakeClass`.
[^2]: You hopefully don't want to know what this is.
[^3]: If you ever see `NULL`, please replace it with `nullptr` in C++11.
[^4]: The difference only occurs if you use PROOF; in that case, the `Slave` methods get run multiple times, once per PROOF host.
[^5]: This acts like a pointer, reading the value when it is dereferenced for the first time.
