# The RDataFrame

The RDataFrame is a fairly new (ROOT 6.14) feature that allows you to schedule operations which happen optimally, and in parallel, after you are done. It does not really invite interactive programming, but it can be used interactively if you know what is in your nTuple and are good at avoiding making mistakes.

First, you need to enable multithreading and the experimental namespace:

{% codetabs name="C++", type="cpp" %}ROOT::EnableImplicitMT();
{% language name="Python", type="py" %}ROOT.ROOT.EnableImplicitMT()
{% endcodetabs %}

Next, you need to create a RDataFrame with your desired tree and file. You can include directories in the tree name:

```cpp
RDataFrame frame("myXiTuple/XiDecayTuple", "DVntuple-March04A.root");
```

Now, you are ready to schedule operations. There are several operations to choose from:

* Transformations: Something that provides a new "view" of the RDataFrame. Filter and custom columns are examples of transformations. 
* Actions: These produce outputs, like histograms or counts. They return a special wrapper around the ROOT class you'd expect that only evaluates the result when you try to use it.
* Instant actions: These immediately evaluate the RDataFrame. For Each is an example of an instant action.

For example, to make an uncut histogram, and to also make a cut and then produce a histogram, you could do:


{% codetabs name="C++", type="cpp" %}auto Xi_M_uncut = frame.Histo1D("Xi_M");
auto filtered = frame.Filter([](Double_t v){return 1305 < v && v < 1340;}, {"Xi_MM"});
auto Xi_M_cut = filtered.Histo1D("Xi_MM");
{% language name="Python", type="py" %}Xi_M_uncut = frame.Histo1D("Xi_M")
filtered = frame.Filter("1305 < Xi_MM && Xi_MM < 1340")
Xi_M_cut = filtered.Histo1D("Xi_MM")
{% endcodetabs %}

> Note for code above: the string given to the python version can also be given to the C++ version if you want to use Cling's JIT instead of lambdas.


Nothing has been calculated yet! You've just scheduled a chain of actions. To run the code, do:

{% codetabs name="C++", type="cpp" %}Xi_M_cut->Draw();
{% language name="Python", type="py" %}Xi_M_cut.Draw()
{% endcodetabs %}

Now you get a result. All actions are performed at this point in the code, so `Xi_M_cut` is available for drawing instantly after this point.

#### Bonus: Column names

You can output the column names like this:

{% codetabs name="C++", type="cpp" %}frame.GetColumnNames();
{% language name="Python", type="py" %}frame.GetColumnNames()
{% endcodetabs %}


This returns a vector of names that you can see in the interactive ROOT console (or manually capture and print in C++).

#### More info

A good resource is the [official documentation](https://root.cern/doc/master/classROOT_1_1RDataFrame.html);
