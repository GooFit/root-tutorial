# Interacting with ROOT

ROOT has an interactive prompt that can be used for quick analysis and checking bits of code. It is a C++11 interpreter, with excellent support for most of the language.

## Starting ROOT

To start ROOT, type `root` into a terminal (assuming your environment is setup correctly). You can add a filename to open if you want to (either a script ending in `.C` or a datafile ending in `.root`). You can add `-l` to skip the startup screen, or `-b` to deactivate graphics entirely ("batch mode").

## Differences compared to standard C++

ROOT adds the following shortcuts to the C++ language:

| Command | Purpose                                           |
|---------|---------------------------------------------------|
| `.ls`   | List the current ROOT items in the ROOT directory (same as `gDirectory->ls()`) |
| `.pwd`  | Show the current ROOT directory (same as `gDirectory->pwd()` |
| `.x file.C` | Run either a script file (one with just `{}` in it) or a function file (function must have the same name as the file) |
| `.L file.C` | Load a file |
|  ...`file.C+` | Compile a file first if needed (can be used most places that take a compilable file) |
| ...`file.C++`| Compile even if already compiled |
| `.q`    | Quit ROOT                                         |


These commands all start with `.`, to make sure they do not conflict with normal C++. ROOT also allows you to directly define a new variable without a type (but this is allowed for historical reasons, and is discouraged, use `auto` instead). ROOT lets you skip the semicolon at the end of every line that C++ requires.

In the ROOT prompt, you can also access variables that are defined in the ROOT object system, but are not defined in C++ (for example, if you open a file). If you are in normal C++, you would have to request a pointer to the variable from the ROOT `gDirectory` service.

ROOT partially supports the standard tab-completion and up-arrow history features, but often it is picky about spaces and whether an object exists yet in C++.

## Directories and ROOT TObjects

ROOT keeps track of its own objects through a directory system[^1]. You start out in an empty directory. If you open a ROOT file (new or existing), you enter that file's "directory". Like a real directory, you can make folders with TDirectory. If you create any ROOT object (they almost all start with T), they get created in the current directory. You can change directory by calling `->cd()` on a file or directory, or by calling `->cd("name")` on the parent file or directory[^2].

ROOT internally stores all ROOT objects using a dictionary with the object names[^3] and pointers to `TObjects`. You can access this using `Get`, but you'll need to "cast" the pointer back to whatever class you actually are actually using[^4]. An example of this:

```cpp
// C++ syntax
auto my_hist = static_cast<TH1D*>(gDirectory->Get("my_hist"));
// Old syntax works too
TH1D *my_hist = (TH1D*) gDirectory->Get("my_hist");
```

This is actually what ROOT is doing in the backround when you try to access something in ROOT that is not defined in C++ yet in the interpreter.

[^1]: Other than similar design, this has nothing to do with directories on your computer.
[^2]: `->cd()` also works to change the active plot.
[^3]: This is why all the objects you make have a name as the first argument, and why you get ugly warnings if you give the same name to two objects.
[^4]: Nobody directly uses `TObject`s, only subclasses. 